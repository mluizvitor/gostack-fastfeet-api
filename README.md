# ![Logo FastFeet by Rocketsear](.gitlab/logo.png)

![Nível de conclusão](https://img.shields.io/badge/Concluído-25%25-orange?style=for-the-badge)

## 📦️ Sobre o projeto

FastFeet é uma aplicação para uma transportadora fictícia de mesmo nome. Esse repositório contem o backend da aplicação, feito em Node.JS.

## ➡️ Progresso

Atualmente o progresso da aplicação encontra-se 25% completo. Como proposto pelo desafio, a aplicação será realizada em 4 partes. As demais, como frontend **mobile** e **web**, serão devidamente referenciadas neste repositório futuramente.

## ⚙️ Como testar

_Nota: Este passo assume que você possui uma configuração do Postgres e Node.JS funcionando corretamente._

Faça o clone do repositório:

```bash
git clone https://gitlab.com/mluizvitor/gostack-fastfeet-api.git
cd gostack-fastfeet-api
```

Instalando dependências:

```bash
yarn
```

ou

```bash
npm install
```

### Instalando Insomnia

Para testar, é preciso instalar o programa [Insomnia](https://insomnia.rest/download/) para fazer as requisições HTTP.
Após a instalação, importe o arquivo [insomnia.json](./insomnia.json) clicando em **Application** > **Preferences** > **Data** > **Import Data** > **From File** > Selecione o arquivo **insomnia.json** na raiz do projeto.

### Iniciando Servidor

Para iniciar o servidor, abra sua aplicação de Terminal na raiz do projeto.

```bash
yarn dev
```

ou

```bash
npm run dev
```

## 🛠️ Ferramentas e Bibliotecas

Para fazer o backend, foram utilizadas as seguintes ferramentas:

- [Node.JS](https://nodejs.org/en/)

- [Express](https://www.npmjs.com/package/express)

- [Sequelize](https://www.npmjs.com/package/sequelize) e [Sequelize-cli](https://www.npmjs.com/package/sequelize-cli)

- [Pg](https://www.npmjs.com/package/pg) e [Pg-hstore](https://www.npmjs.com/package/pg-hstore)

- [Bcryptjs](https://www.npmjs.com/package/bcryptjs)

- [JsonWebToken](https://www.npmjs.com/package/jsonwebtoken)

- [Nodemon](https://www.npmjs.com/package/nodemon)

- [Sucrase](https://www.npmjs.com/package/sucrase)

- [Eslint](https://www.npmjs.com/package/eslint)

- [Prettier](https://www.npmjs.com/package/prettier)

## 📃️ Licença

Este repositório está sob o [MIT License](./LICENSE).

> Feito com ❤️ por Luiz Vitor Monteiro
