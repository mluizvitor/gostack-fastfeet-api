import express from 'express';
import routes from './router';

import './database';

const app = express();

app.use(express.json());
app.use(routes);

export default app;
