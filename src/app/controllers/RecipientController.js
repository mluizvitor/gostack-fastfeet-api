import * as Yup from 'yup';

import Recipient from '../models/Recipient';

class RecipientController {
  async index(req, res) {
    const recipients = await Recipient.findAll({
      attributes: [
        'id',
        'name',
        'street',
        'number',
        'complement',
        'state',
        'city',
        'zip_code',
      ],
    });

    return res.json(recipients);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      street: Yup.string().required(),
      number: Yup.number()
        .positive()
        .required(),
      complement: Yup.string().notRequired(),
      state: Yup.string().required(),
      city: Yup.string().required(),
      zip_code: Yup.number()
        .positive()
        .required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validação falhou' });
    }

    const { number, zip_code } = req.body;

    const recipientExists = await Recipient.findOne({
      where: { number, zip_code },
    });

    if (recipientExists) {
      return res.status(400).json({ error: 'Destinatário já cadastrado' });
    }

    const {
      id,
      name,
      street,
      complement,
      state,
      city,
    } = await Recipient.create(req.body);

    return res.json({
      id,
      name,
      street,
      number,
      complement,
      state,
      city,
      zip_code,
    });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string(),
      street: Yup.string(),
      number: Yup.number().positive(),
      complement: Yup.string(),
      state: Yup.string(),
      city: Yup.string(),
      zip_code: Yup.number().positive(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validação falhou' });
    }

    const { zip_code, number } = req.params;

    const recipient = await Recipient.findOne({ where: { zip_code, number } });

    if (!recipient) {
      return res.status(400).json({ error: 'Destinatário não cadastrado' });
    }

    const {
      id,
      name,
      street,
      complement,
      state,
      city,
    } = await recipient.update(req.body);

    return res.json({
      id,
      name,
      street,
      number,
      complement,
      state,
      city,
      zip_code,
    });
  }

  async delete(req, res) {
    const { zip_code, number } = req.params;

    const recipient = await Recipient.findOne({ where: { zip_code, number } });

    if (!recipient) {
      return res.status(400).json({ error: 'Destinatário não cadastrado' });
    }

    await recipient.destroy();

    return res.send();
  }
}

export default new RecipientController();
