import User from '../models/User';

class UserController {
  async index(req, res) {
    const user = await User.findAll({
      attributes: ['id', 'name', 'email'],
    });

    return res.json(user);
  }
}

export default new UserController();
