import { Router } from 'express';
import authMiddleware from './app/middlewares/auth';
import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import RecipientController from './app/controllers/RecipientController';

const routes = new Router();

routes.post('/session', SessionController.store);

// Middleware de autenticação
routes.use(authMiddleware);

routes.get('/users', UserController.index);

routes.get('/recipient', RecipientController.index);
routes.post('/recipient', RecipientController.store);
routes.put('/recipient/:zip_code/:number', RecipientController.update);
routes.delete('/recipient/:zip_code/:number', RecipientController.delete);

export default routes;
